# Landing Page
This is an **App Landing Page** for Arsnova Cards Based on *Bootstrap New Age Theme*. 
It was modified, to conform with accessiblity standards and have a progressive look and feel to it. 
The biggest changes to the site are new images on mockup devices, alt lables for those images. A cookie consent bar. And The overall Descriptions
of the product with custom links to terms and licence and site notice.