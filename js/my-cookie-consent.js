function acceptCookies() {
    //create cookie that stores descision
    document.getElementById("cookieconsent").hidden = true;
    console.log("Cookies Accepted!");
    document.cookie = 'cookieconsent=true';
}

//this code section is relevent if there is a decline option present
/*
function declineCookies() {
  document.getElementById("cookieconsent").hidden = true;
  console.log("Cookies Declined!");
  document.cookie = 'cookieconsent = false'

}
*/

//Fade Cookie consent popup, if consent was not given yet previously
$("#cookieconsent").hide();

if(document.cookie) {
    console.log(document.cookie);

    //check if cookie exists
    if (document.cookie == "cookieconsent=true") {
        //leave hidden if consent is given
    } else {
        $(document).ready(function () {
            $("#cookieconsent").fadeIn(1500);
        });
    }
}else{
    //if none exists, create consent false cookie
    document.cookie = "cookieconsent=false";
}