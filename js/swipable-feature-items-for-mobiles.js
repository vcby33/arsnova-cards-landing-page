//Check if User agent is that of a mobile user and apply the swipable features if so
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    //Function to detect swipe with swipe details like direction
    $("h1").swipe( {
        swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
            $(this).text("Direction Swiped in: " + direction );
        }
    });
}

