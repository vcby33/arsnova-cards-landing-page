//Timer that runs function in given interval (ms)
var typer= window.setInterval(typeNextLetter, 35);
i = 0;
var text = "Intuitive, fun and smart learning with Arsnova Cards!";
function typeNextLetter() {
    //abort when finished
    if(i >= text.length) {
        window.clearInterval(typer);
    }
    //Keep adding current letter of String
    $("h1").append(text[i]);
    i++;
}